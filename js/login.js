const btn = document.querySelector(".login__btn");
const emal = document.querySelector(".login__email");
const pass = document.querySelector(".login__password");

spinIcon = document.querySelector(".spinner"),
btnText = document.querySelector(".btn-text");

// btn
btn.addEventListener("click", (e) => {
  e.preventDefault();
  const getUserData = JSON.parse(localStorage.getItem("userdata"));
  if (!getUserData) return alert("siz ro'yhatdan o'tmagansiz !");

  if (emal.value && pass.value) {
    if (emal.value == getUserData[0] && pass.value == getUserData[1]) {
            btn.style.cursor = "wait";
            btn.classList.add("checked");
            spinIcon.classList.add("spin");
            btnText.textContent = "Loading";

            setTimeout(() => {
              btn.style.pointerEvents = "none";
              spinIcon.classList.replace("spinner", "check");
              spinIcon.classList.replace("fa-circle-notch", "fa-check");
              btnText.textContent = "Done";
            }, 3000); 

          setTimeout(() => {
            window.location.href = "main.html";
          }, 4000);
    } else {
      alert("Parol yoki username xato 🤔");
    }
  } else {
    alert("Iltimos Malmotlarni tuliq tuldiring 🤔");
  }
});




