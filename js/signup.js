const btn = document.querySelector(".button");
const emal = document.querySelector(".email");
const pass = document.querySelector(".password");
const pass_confirm = document.querySelector(".confirm");
spinIcon = document.querySelector(".spinner"),
btnText = document.querySelector(".btn-text");


let data = [];

btn.addEventListener("click", (e) => {
  e.preventDefault();
  if (emal.value && pass.value && pass_confirm.value) {
    if (pass.value == pass_confirm.value) {
            btn.style.cursor = "wait";
            btn.classList.add("checked");
            spinIcon.classList.add("spin");
            btnText.textContent = "Loading";

            setTimeout(() => {
              btn.style.pointerEvents = "none";
              spinIcon.classList.replace("spinner", "check");
              spinIcon.classList.replace("fa-circle-notch", "fa-check");
              btnText.textContent = "Done";
            }, 3000); 
            
      data.push(emal.value, pass_confirm.value);
      localStorage.setItem("userdata", JSON.stringify(data));
      setTimeout(()=>{
        window.location.href = "main.html";
      },4000)
    } else {
      alert("Parollar mos emas 🤔");
    }
  } else {
    alert("Iltimos malumotlarni tuliq tuldiring 🤔");
  }
});
                
            
